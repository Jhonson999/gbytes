

import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import productRouter from './routers/productRouter.js';
import userRouter from './routers/userRouters.js';
import orderRouter from './routers/orderRouter.js';
import path from 'path';
import uploadRouter from './routers/uploadRouter.js';

dotenv.config();


const port = process.env.PORT || 5000

const app = express();

//parsing JSON data in the body of request
app.use(express.json());
app.use(express.urlencoded({extended: true}));


mongoose.connect('mongodb+srv://admin:admin131@zuittbootcamp.mbqhe.mongodb.net/GyverShop?retryWrites=true&w=majority',

  {

    useNewUrlParser: true,
    useUnifiedTopology: true
  }

  );



//(path,responder)
app.use('/api/uploads', uploadRouter);
app.use('/api/users', userRouter);
app.use('/api/orders', orderRouter);
app.use('/api/products', productRouter);
app.get('/api/config/paypal', (req, res) => {
  res.send(process.env.PAYPAL_CLIENT_ID || 'sb');
});
const __dirname = path.resolve();
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));
app.get("/",(req,res) => {
 res.send('Server is ready');
});

// to show error to users
app.use((err, req, res, next) => {
  res.status(500).send({ message: err.message });
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"))

db.once('open',() => console.log('Connected to the cloud database'))





  app.listen(port, () => console.log(`Server is running at port ${port}`))