import express from 'express';
import expressAsyncHandler from 'express-async-handler';
import Order from '../models/orderModel.js';
import { isAuth } from '../utils.js';

const orderRouter = express.Router();

orderRouter.get(
  '/mine',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const orders = await Order.find({ user: req.user._id });
    res.send(orders);
  })
);


orderRouter.post(
  '/',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    //TO CHECK IF ORDER ITEMS CONTAINS ITEMS OR NOT
    if (req.body.orderItems.length === 0) {
    //IF CART IS EMPTY
      res.status(400).send({ message: 'Cart is empty' });
    } else {
    //IF CART IS NOT EMPTY, WE CAN PROCEED TO CREATE ORDER
      const order = new Order({
        orderItems: req.body.orderItems,
        shippingAddress: req.body.shippingAddress,
        paymentMethod: req.body.paymentMethod,
        itemsPrice: req.body.itemsPrice,
        shippingPrice: req.body.shippingPrice,
        taxPrice: req.body.taxPrice,
        totalPrice: req.body.totalPrice,
        user: req.user._id,
      });
      //TO SAVE ORDER IN THE DATABASE
      const createdOrder = await order.save();
      res
        .status(201)
        .send({ message: 'New Order Created', order: createdOrder });
    }
  })
);
 //TO GET ORDER DETAILS FROM BACKEND
 orderRouter.get(
  '/:id',
  //ONLY AUTHENTICATED USERS CAN SEE ORDER DETAILS
  isAuth,
  // expressAsyncHandler -> TO CATCH ANY ERRORS IN ASYNC FUNCTIONS
  expressAsyncHandler(async (req, res) => {
    const order = await Order.findById(req.params.id);
    if (order) {
    //IF ORDER EXISTS
      res.send(order);
    } else {
      //IF ORDER DOESN'T EXIST
      res.status(404).send({ message: 'Order Not Found' });
    }
  })
);

orderRouter.put(
  '/:id/pay',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const order = await Order.findById(req.params.id);
    if (order) {
      order.isPaid = true;
      order.paidAt = Date.now();
      order.paymentResult = {
        id: req.body.id,
        status: req.body.status,
        update_time: req.body.update_time,
        email_address: req.body.email_address,
      };
      const updatedOrder = await order.save();
      res.send({ message: 'Order Paid', order: updatedOrder });
    } else {
      res.status(404).send({ message: 'Order Not Found' });
    }
  })
);

export default orderRouter;